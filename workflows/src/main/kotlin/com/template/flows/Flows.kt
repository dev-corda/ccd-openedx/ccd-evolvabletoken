package com.template.flows

import co.paralleluniverse.fibers.Suspendable
import com.r3.corda.lib.tokens.contracts.states.EvolvableTokenType
import com.r3.corda.lib.tokens.contracts.states.NonFungibleToken
import com.r3.corda.lib.tokens.contracts.types.IssuedTokenType
import com.google.common.collect.ImmutableList
import com.r3.corda.lib.tokens.contracts.utilities.getAttachmentIdForGenericParam
import com.r3.corda.lib.tokens.workflows.flows.rpc.CreateEvolvableTokens
import com.r3.corda.lib.tokens.workflows.flows.rpc.IssueTokens
import com.r3.corda.lib.tokens.workflows.flows.rpc.MoveNonFungibleTokens
import com.r3.corda.lib.tokens.workflows.types.PartyAndToken
import com.template.states.House
import net.corda.core.contracts.TransactionState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.crypto.SecureHash
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.StartableByRPC
import net.corda.core.identity.Party
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction
import net.corda.core.utilities.ProgressTracker
import java.util.*


@StartableByRPC
class CreateHouseToken() : FlowLogic<SignedTransaction>() {
    override val progressTracker = ProgressTracker()

    @Suspendable
    override fun call(): SignedTransaction {
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val evolvableTokenType = House("KBA", ourIdentity,0, linearId = UniqueIdentifier())
        val transactionState = TransactionState(evolvableTokenType, notary = notary)
        return subFlow(CreateEvolvableTokens(transactionState))
    }
}

@StartableByRPC
class IsssueHouseToken(
        val tokenID :String,
        val recipient: Party
): FlowLogic<SignedTransaction>(){
    @Suspendable
    override fun call(): SignedTransaction {
        val uuid= UUID.fromString(tokenID)
        val queryCriteria= QueryCriteria.LinearStateQueryCriteria(uuid = listOf(uuid))
        val tokenStateAndRef = serviceHub.vaultService.queryBy<EvolvableTokenType>(queryCriteria).states.single()
        val token = tokenStateAndRef.state.data.toPointer<EvolvableTokenType>()
        val issueTokenType= IssuedTokenType(ourIdentity,token)
        val tokenTypeJarHash: SecureHash? = issueTokenType.getAttachmentIdForGenericParam()
        val nonFungibleToken = NonFungibleToken(issueTokenType,recipient, UniqueIdentifier(),tokenTypeJarHash)
        return subFlow(IssueTokens(ImmutableList.of(nonFungibleToken)))
    }
}

@StartableByRPC
class MoveTokenFlow(val tokenID: String, val newOwner: Party): FlowLogic<SignedTransaction>(){
    @Suspendable
    override fun call(): SignedTransaction {

        val uuid= UUID.fromString(tokenID)
        val queryCriteria= QueryCriteria.LinearStateQueryCriteria(uuid = listOf(uuid))
        val tokenStateAndRef = serviceHub.vaultService.queryBy<EvolvableTokenType>(queryCriteria).states.single()
        val token = tokenStateAndRef.state.data.toPointer<EvolvableTokenType>()
        return subFlow(MoveNonFungibleTokens(PartyAndToken(newOwner,token)))
    }
}
