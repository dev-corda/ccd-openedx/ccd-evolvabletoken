# Evolvable Token Cordapp

To run the Cordapp follow the below steps:

## 1. Build the project

Windows: `gradlew deployNodes`  

Ubuntu: `./gradlew deployNodes`  

## 2. Run the network

Windows: `build\nodes\runnodes.bat`  

Ubuntu: `build/nodes/runnodes`  

## 3. Sample Transaction

Select PartyA node console and enter the following command to create House Token:  

`start CreateHouseToken`

Select PartyA node console and enter the following command to issue House Token:  

`start IsssueHouseToken tokenID: <linear id>, recipient: PartyB`

Select PartyB node console and enter the following command to move House Token:  

`start MoveTokenFlow tokenID: <linear id>, newOwner: PartyC`