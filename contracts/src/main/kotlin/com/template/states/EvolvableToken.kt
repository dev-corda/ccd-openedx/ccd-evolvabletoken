
package com.template.states

import com.r3.corda.lib.tokens.contracts.states.EvolvableTokenType

import com.template.contracts.TemplateContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party

@BelongsToContract(TemplateContract::class)
data class House(
        val address: String,
        val maintainer: Party,
        override val fractionDigits: Int,
        override val linearId: UniqueIdentifier
) : EvolvableTokenType(){
    companion object {
        val contractId = this::class.java.enclosingClass.canonicalName
    }
    override val maintainers: List<Party> get() = listOf(maintainer)
}